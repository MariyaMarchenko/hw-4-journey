let but = document.getElementById('but-theme');

but.addEventListener('click', () =>{
  const currTheme=  localStorage.getItem('theme');
  if (currTheme === 'RED'){
      document.getElementsByClassName('square')[0].removeAttribute('theme', 'RED');
      document.querySelector('.bottom-button').removeAttribute('theme', 'RED');
      localStorage.setItem('theme', 'GREEN');
  } else {
      localStorage.setItem('theme', 'RED');
      document.getElementsByClassName('square')[0].setAttribute('theme', 'RED');
      document.querySelector('.bottom-button').setAttribute('theme', 'RED');
  }
} );